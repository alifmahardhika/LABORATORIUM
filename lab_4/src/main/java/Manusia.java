//Nama: Alif Mahardhika; NPM: 1706021934; DDP2 Kelas E; Last Edited: 28/02/2018 (9.37);
import java.util.ArrayList;

public class Manusia{
    //atribut
    private String nama;
    private int umur;
    private int uang = 50000;
    private float kebahagiaan = 50;
    private boolean meninggal = false;
    private static ArrayList<Manusia> listManusia = new ArrayList<Manusia>();

    //konstruktor
    public Manusia(String nama){
      this.nama = nama;
      listManusia.add(this);
    }
    public Manusia(String nama, int umur){
      this.nama = nama;
      this.umur = umur;
      listManusia.add(this);
    }
    public Manusia(String nama, int umur, int uang){
      this.nama = nama;
      this.umur = umur;
      this.uang = uang;
      listManusia.add(this);
    }
    public Manusia(String nama, int umur, int uang, float kebahagiaan){
      this.nama = nama;
      this.umur = umur;
      this.uang = uang;
      this.kebahagiaan = kebahagiaan;
      listManusia.add(this);
    }

    //SETTER & GETTER
    public void setNama (String nama){
      this.nama = nama;
    }
    public void setUmur (int umur){
      this.umur = umur;
    }
    public void setUang (int uang){
      this.uang = uang;
    }
    public void setKebahagiaan (float kebahagiaan){
      //set batas maksimal dan minimal kebahagiaan
      if (kebahagiaan > 100) {
        this.kebahagiaan = 100;
      }else if (kebahagiaan < 0 ) {
        this.kebahagiaan = 0;
      }else {
        this.kebahagiaan = kebahagiaan;
      }
    }
    public void setMeninggal(boolean meninggal) {
    	this.meninggal = meninggal;
    }
    public String getNama(){
      return nama;
    }
    public int getUmur(){
      return umur;
    }
    public int getUang(){
      return uang;
    }
    public float getKebahagiaan(){
      return kebahagiaan;
    }
    public boolean getMeninggal() {
    	return meninggal;
    }
    //methods
    //beri uang tanpa parameter jumlah
    public void beriUang (Manusia penerima){
      if (meninggal == true) {        //jika sudah meninggal maka tidak dapat bagibagi uang
    		System.out.format("%s telah tiada\n", nama);
    	}else{
        if (penerima.meninggal == false) {        //jika belum meninggal maka dapat diberi uang
          //cari nilai ascii perkarakter nama lalu diproses sesuai rumus
          int jumlah = 0;
          for (int n = 0; n < penerima.getNama().length(); n++){
      			char nilai= penerima.getNama().charAt(n);
      			int ascii = (int)nilai;
            jumlah += (ascii*100);
          }
          if (uang > jumlah) {        //jika uang pemberi cukup
  					uang -= jumlah;
  					setKebahagiaan(getKebahagiaan() + (float) jumlah / 6000);
  					penerima.setUang(penerima.getUang() + jumlah);
  					penerima.setKebahagiaan(penerima.getKebahagiaan() + ((float)jumlah / 6000));
  					System.out.format("%s memberi uang sebanyak %d kepada %s, mereka berdua senang \n", nama, jumlah, penerima.getNama());
  				}else{        //jika uang pemberi tidak cukup
  					System.out.format("%s ingin memberi uang kepada %s namun tidak memiliki cukup uang \n", nama, penerima.getNama());
  				}
        } else {
  				System.out.format("%s telah tiada\n", penerima.getNama());        //jika sudah meninggal maka tidak dapat diberi uang
  			}
      }
    }
    //beri uang ke seorang penerima dengan jumlah tertentu
    public void beriUang (Manusia penerima, int jumlah){
      if (meninggal == true) {
    		System.out.format("%s telah tiada\n", nama);
    	}else{
    		if (penerima.meninggal == false) {
				if (uang > jumlah) {
					uang -= jumlah;
					setKebahagiaan(getKebahagiaan() + (float) jumlah / 6000);
					penerima.setUang(penerima.getUang() + jumlah);
					penerima.setKebahagiaan(penerima.getKebahagiaan() + ((float)jumlah / 6000));
					System.out.format("%s memberi uang sebanyak %d kepada %s, mereka berdua senang\n", nama, jumlah, penerima.getNama());
				}else{
					System.out.format("%s ingin memberi uang kepada %s namun tidak memiliki cukup uang\n", nama, penerima.getNama());
				}
			 }else{
			    System.out.format("%s telah tiada\n", penerima.getNama());
			  }
		 }
   }
   //method bekerja
    public void bekerja (int durasi, int bebanKerja){
      if (meninggal == true) {        //jika sudah meninggal maka tidak bisa bekerja
    		System.out.format("%s telah tiada\n", nama);
    	}else{
        if (umur < 18){       //jika umur dobawah 18
          System.out.format("%s belum boleh bekerja karena masih dibawah umur\n", nama);
        }else {
          int bebanKerjaTotal = durasi*bebanKerja;
          if (bebanKerjaTotal<=kebahagiaan){      //kerja yang normal gitu deh
            kebahagiaan -= bebanKerjaTotal;
            int pendapatan = bebanKerjaTotal*10000;
            uang += pendapatan;
            if (kebahagiaan < 0){
              kebahagiaan = 0;
            }
            System.out.format("%s bekerja full time, total pendapatan : %d\n", nama, pendapatan);
          }else{        //jika manusianya kurang bahagia jadi tidak bisa kerja fulltime
            int durasiBaru = (int) kebahagiaan/bebanKerja;
            int totalBebanKerja = durasiBaru*bebanKerja;
            int pendapatan = totalBebanKerja*10000;
            uang += pendapatan;
            kebahagiaan -= totalBebanKerja;
          /*  if (kebahagiaan < 0){
              kebahagiaan = 0;
            }*/
            System.out.format("%s tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : %d\n", nama, pendapatan);
          }
        }
      }
    }
    //method rekreasi
    public void rekreasi(String namaTempat){
      if (meninggal == true) {
    		System.out.format("%s telah tiada\n", nama);
    	}else{
	    	int biaya = namaTempat.length() * 10000;
	    	if (uang > biaya) {
	    		uang -= biaya;
	    		setKebahagiaan(getKebahagiaan() + namaTempat.length());
	    		System.out.format("%s berekreasi di %s, %s senang\n", nama, namaTempat, nama);
	    	}else{
	    		System.out.format("%s tidak mempunyai cukup uang untuk berekreasi di %s\n", nama, namaTempat);
	    	}
    	}
    }
    //method sakit
    public void sakit(String namaPenyakit){
      if (meninggal == true) {
        System.out.format("%s telah tiada\n", nama);
      }else{
      setKebahagiaan(getKebahagiaan() - namaPenyakit.length());
        System.out.format("%s terkena penyakit %s\n", nama, namaPenyakit);
      }
    }
    //method panggil manusia
    public String toString(){
      if (kebahagiaan > 100){
        kebahagiaan = 100;
      }
      if (kebahagiaan < 0){
        kebahagiaan = 0;
      }
      String hasil = "Nama\t\t: "+nama+"\nUmur\t\t: "+umur+"\nUang\t\t: "+uang+"\nKebahagiaan\t: "+kebahagiaan;
      return hasil;
    }
    //method meninggal
    public void meninggal() {
    	if (meninggal == true){
    		System.out.format("%s telah tiada\n", nama);
    	}else{
	    	setMeninggal(true);
	    	System.out.format("%s meninggal dengan tenang, kebahagiaan : %f\n", nama, kebahagiaan);
	    	Manusia manusiaTerakhir = listManusia.get(listManusia.size() - 1);
	    	if (manusiaTerakhir.meninggal == true) {
	    		System.out.format("Semua harta %s hangus\n", nama);
	    	}else{
		    	manusiaTerakhir.setUang(manusiaTerakhir.getUang() + uang);
		    	setUang(0);
		    	System.out.format("Semua harta %s disumbangkan untuk %s\n", nama, manusiaTerakhir.getNama());
		    	setNama("Almarhum " + nama);
	    	}
	    }
    }
}
