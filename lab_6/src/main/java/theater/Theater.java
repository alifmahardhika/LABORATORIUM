//Alif Mahardhika; 1706021934
package theater;                //declaring package name
//importing the packages needed
import java.util.ArrayList;
import movie.*;
import ticket.*;

public class Theater {              //creating the class "Theater"

    //attributes
	private String name;
	private Movie[] movies;
	private ArrayList<Ticket> tickets;
	private int balance;
	private static int totalRevenueOfTheaters;

    //constructors
	public Theater(String name, int balance, ArrayList<Ticket> tickets, Movie[] movies) {
		this.name = name;
		this.balance = balance;
		this.tickets = tickets;
		this.movies = movies;
	}

    //setter and getter
	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public int getBalance() {
		return balance;
	}

	public void setTickets(ArrayList<Ticket> tickets) {
		this.tickets = tickets;
	}

	public ArrayList<Ticket> getTickets() {
		return tickets;
	}

	public void setMovies(Movie[] movies) {
		this.movies = movies;
	}

	public Movie[] getMovies() {
		return movies;
	}

    //methods
	public void printInfo() {              //printing out the informations
		String temp = "";
        //looping to iterate all movie names from the movie list
		for (int i = 0; i < movies.length; i++) {
			temp += movies[i].getTitle() + ", ";
			if (i == movies.length - 1) {
				temp = temp.substring(0, temp.length() - 2);
			}
		}
		System.out.println("------------------------------------------------------------------\n" +
				"Bioskop                 : " + name + "\n" +
				"Saldo Kas               : " + balance + "\n" +
				"Jumlah tiket tersedia   : " + String.valueOf(tickets.size()) + "\n" +
				"Daftar Film tersedia    : " + temp + "\n" +
				"------------------------------------------------------------------");
	}

	public static void printTotalRevenueEarned(Theater[] theaters) {   //printing the revenue earned
        //looping to iterate all theatre's balance fromthe theater list
        for (int i = 0; i < theaters.length; i++) {
			totalRevenueOfTheaters += theaters[i].balance;        //counting total revenue from all theaters
		}
		System.out.println("Total uang yang dimiliki Koh Mas : Rp. " + totalRevenueOfTheaters + "\n" +
						   "------------------------------------------------------------------");
        //looping to iterate all theater name and balance from theater list
        for (int i = 0; i < theaters.length; i++) {
			System.out.println("Bioskop         : " + theaters[i].name + "\n" +
							   "Saldo Kas       : Rp. " + theaters[i].balance + "\n");
		}
		System.out.println("------------------------------------------------------------------");
	}
}
