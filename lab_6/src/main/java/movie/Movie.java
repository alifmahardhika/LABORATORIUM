//Alif Mahardhika; 1706021934
package movie;              //declaring the package name

public class Movie {        //creating the class "Movie"

    //attributes
    private String title, rating, genre, type;
    private int duration;

    //constructor
    public Movie(String title, String rating, int duration, String genre, String type) {
        this.title = title;
        this.rating = rating;
        this.duration = duration;
        this.genre = genre;
        this.type = type;
    }

    //setter and getter
    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getRating() {
		return rating;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public int getDuration() {
		return duration;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getGenre() {
		return genre;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

    //methods
	public String info() {             //method to print info
		return "------------------------------------------------------------------\n" +
			   "Judul   : " + title + "\n" +
			   "Genre   : " + genre + "\n" +
			   "Durasi  : " + String.valueOf(duration) + " menit\n" +
			   "Rating  : " + rating + "\n" +
			   "Jenis   : Film " + type + "\n" +
			   "------------------------------------------------------------------";
	}
/*
	//BONUS
	public boolean equals(Movie movie) {
	    if (movie == null) {
	        return false;
        } else if (movie == this) {
            return true;
        } return false;
    }*/
}
