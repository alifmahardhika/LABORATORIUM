//Alif Mahardhika; 1706021934
package customer;
import ticket.*;
import theater.*;

public class Customer {

    private String name;
    private int age;
    private boolean isFemale;
    //private Ticket latestBoughtTicket; //BONUS

    public Customer(String name, boolean isFemale, int age) {
        this.name = name;
        this.isFemale = isFemale;
        this.age = age;
    }

    public Ticket orderTicket(Theater theater, String movieName, String movieDay, String movieDimension) {
        String theaterName = theater.getName();
        for (int i = 0; i < theater.getTickets().size(); i++) {
            Ticket ticket = theater.getTickets().get(i);
            String movieTitle = theater.getTickets().get(i).getMovie().getTitle();
            String movieRating = theater.getTickets().get(i).getMovie().getRating();
            String ticketDay = theater.getTickets().get(i).getDay();
            int ticketPrice = theater.getTickets().get(i).getPrice();
            int ageLimit = 0;
            boolean is3D;
            // checking if the  movie name is correct
            if (movieTitle.equals(movieName)) {
                // check if the movie day is correct
                if (ticketDay.equals(movieDay)) {
                    //checking whether the movie dimensions is in the list
                    if (movieDimension.equals("3 Dimensi")) {
                        is3D = true;
                    } else {
                        is3D = false;
                    }
                    if (theater.getTickets().get(i).is3D() == is3D) {
                        //checking age restrictions
                        if (movieRating.equals("Remaja")) {
                            ageLimit = 13;
                        } else if (movieRating.equals("Dewasa")) {
                            ageLimit = 17;
                        }
                        if (age < ageLimit) {
                            System.out.format("%s masih belum cukup umur untuk menonton %s dengan rating %s%n", name, movieName, movieRating);
                            return null;
                        }
                        System.out.format("%s telah membeli tiket %s jenis %s di %s pada hari %s seharga Rp. %d%n", name, movieName, movieDimension, theaterName, movieDay, ticketPrice);
                        theater.setBalance(theater.getBalance() + ticketPrice);
                        //this.latestBoughtTicket = ticket; //BONUS
                        return ticket;
                    }
                }
            }
        }
        System.out.format("Tiket untuk film %s jenis %s dengan jadwal %s tidak tersedia di %s%n", movieName, movieDimension, movieDay, theaterName);
        return null;
    }

    public void findMovie(Theater theater, String movieName) {
        String theaterName = theater.getName();
        for (int i = 0; i < theater.getMovies().length; i++) {
            String movieTitle = theater.getMovies()[i].getTitle();
            if (movieTitle.equals(movieName)) {
                System.out.println(theater.getMovies()[i].info());
                return;
            }
        }
        System.out.format("Film %s yang dicari %s tidak ada di bioskop %s%n", movieName, name, theaterName);
    }
/*
    //BONUS
    public void cancelTicket(Theater theater) {
        String theaterName = theater.getName();
        String ticketMovieTitle = latestBoughTicket.getMovie().getTitle();
        String ticketMovieDimension = latestBoughTicket.getMovie().getType();
        String ticketDay = latestBoughTicket.getDay();
        int ticketMovieDuration = latestBoughTicket.getMovie().getDuration();
        int ticketPrice = latestBoughTicket.getPrice();
        for (int i = 0; i < theater.getTickets().size(); i++) {
            String movieTitle = theater.getTickets().get(i).getMovie().getTitle();
            String movieDimension = theater.getTickets().get(i).getMovie().getType();
            String movieDay = theater.getTickets().get(i).getDay();
            if (movieTitle.equals(ticketMovieTitle)) {
                if (movieDay.equals(ticketDay)) {
                    if (movieDimension.equals(ticketMovieDimension)) {
                        theater.setBalance(theater.getBalance() - ticketPrice);
                        System.out.format("Tiket film %s dengan waktu tayang %s jenis %s dikembalikan ke bioskop %s%n", ticketMovieTitle, String.valueOf(ticketMovieDuration), ticketMovieDimension, theaterName);
                        return;
                    }
                }
            }
            System.out.format("Maaf tiket tidak bisa dikembalikan, %s tidak tersedia dalam %s", ticketMovieTitle, theaterName);
        }
    }

    public void watchMovie(Ticket ticket) {
        ;
    }*/
}
