//Alif Mahardhika; 1706021934
package ticket;         //declaring the package name
import movie.*;         //importing the movie package

public class Ticket {   //creating the class "Ticket"

    //attributes
	private Movie movie;
	private String day;
	private boolean is3D;
	private int price;

    //constructors
	public Ticket(Movie movie, String day, boolean is3D) {
		this.movie = movie;
		this.day = day;
		this.is3D = is3D;
		this.price = 60000;
		setPrice(this.price);
	}
    //setter and getter
	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getDay() {
		return day;
	}

	public void setType(boolean is3D) {
		this.is3D = is3D;
	}

	public boolean is3D() {
		return is3D;
	}

	public void setPrice(int price) {          //setting how the price changes
		if (day.equals("Sabtu") || day.equals("Minggu")) {        //during the weekends
			this.price += 40000;
		}
		if (is3D) {                //if the ticket type is 3D
			this.price += 0.2 * this.price;
		}
	}

	public int getPrice() {
		return price;
	}

    //methods
	public String info() {         //printing out informations about the ticket
		String type;
		if (this.is3D) {      //ticket type
			type = "3 Dimensi";
		} else {
			type = "Biasa";
		}
		return "------------------------------------------------------------------\n" +
			   "Film            : " + movie.getTitle() + "\n" +
			   "Jadwal Tayang   : " + day + "\n" +
			   "Jenis           : " + type + "\n" +
			   "------------------------------------------------------------------\n";
	}
}
