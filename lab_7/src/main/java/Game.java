import character.*;
import java.util.ArrayList;

public class Game{
    ArrayList<Player> player = new ArrayList<Player>();
    ArrayList<Monster> monster = new ArrayList<Monster>();

    /**
     * Fungsi untuk mencari karakter
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name){
        boolean adaGa = false;
        Player temp = null;
        for (Player pemain : player){
            if (pemain.getName().equals(name)){
                adaGa = true;
                temp = pemain;
            }
        }
        if (adaGa == true) return temp;
        else return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp){
        System.out.println("==============================================================");
        if (this.find(chara) != null){
            return "Sudah ada karakter bernama "+ chara;
        } else if(tipe.equalsIgnoreCase("Human")){
            Human manusia = new Human(chara, hp);
            if (manusia.getHp()<=0){
                manusia.setHp(0);
                manusia.setDead();
            }
            player.add(manusia);
            return chara + " ditambah ke game";
        } else if(tipe.equalsIgnoreCase("Magician")){
            Magician pesulap = new Magician(chara, hp);
            if (pesulap.getHp()<=0){
                pesulap.setHp(0);
                pesulap.setDead();
            }
            player.add(pesulap);
            return chara + " ditambah ke game";
        } else if (tipe.equalsIgnoreCase("Monster")){
            return add(chara, tipe, hp, "AAAAAAaaaAAAAAaaaAAAAAA" );
        } else return "Tidak ada tipe "+tipe;
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar){
        boolean udahAdaBlm = false;
        Monster temp = null;
        for(Monster hmAdaGakYa:monster){
            if(hmAdaGakYa.getName().equalsIgnoreCase(chara)){
                udahAdaBlm =true;
                temp = hmAdaGakYa;
            }
        }
        if (udahAdaBlm == true) monster.remove(monster.indexOf(temp));
        if (tipe.equalsIgnoreCase("monster")){
            Monster seram = new Monster(chara, hp);
            if (seram.getHp()<=0){
                seram.setHp(0);
                seram.setDead();
            }
            player.add(seram);
            monster.add(seram);
            return chara + " ditambah ke game";
        } else {
            return "Tipe "+tipe+" tidak dapat melakukan roar";
        }
    }

    /**
     * fungsi untuk menghapus character dari game
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara){
        System.out.println("==============================================================");
        String temp = "";
        boolean adaGa = false;
        for (int i = 0; i<player.size();i++){
             String nameToRemove = player.get(i).getName();
             if (nameToRemove.equalsIgnoreCase(chara)){
                 player.remove(i);

                 for(Player ilang:player){
                    if (ilang.getName().equalsIgnoreCase(chara)&&ilang.getTipe().equalsIgnoreCase("monster")) monster.remove(monster.indexOf(ilang));
                 }

                 temp+= chara + " dihapus dari game";
                 adaGa = true;
             }
        }
        if (adaGa==true) return temp;
        else return chara +" tidak ada";
    }


    /**
     * fungsi untuk menampilkan status character dari game
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara){
        System.out.println("==============================================================");
        boolean adaGa = false;
        String result = "";
        for(Player pemain: player){
            if (pemain.getName().equalsIgnoreCase(chara)){
                result += pemain.status();
                adaGa = true;
            }
        }if(!adaGa) return chara+" tidak ada";
        else return result;
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status(){
        System.out.println("==============================================================");
        String result = "";
        for(Player pemain:player){
            //System.out.println("------------------------------------");
            result+= pemain.status()+"\n";
        }
        return result;
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara){
        System.out.println("==============================================================");
        boolean adaGa = false;
        String result= "";
        for(Player pemain:player){
            if (pemain.getName().equalsIgnoreCase(chara)){
                //System.out.println("------------------------------------");
                adaGa =true;
                result+= pemain.diet();
            }

        }if (adaGa==false) return  chara + " tidak ada";
        else return result;

    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
        System.out.println("==============================================================");
        for(Player pemain:player){
            pemain.diet();
        }
        return "";
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName){
        System.out.println("==============================================================");
        String result ="";
        for(Player penyerang:player){
            if (penyerang.getName().equals(meName)){
                for(Player diserang:player){
                    if (diserang.getName().equals(enemyName)){
                        penyerang.generalAttack(diserang);
                        if(diserang.getHp()<=0){
                            diserang.setHp(0);
                            diserang.setDead();
                        }
                        result += "Nyawa "+ diserang.getName() +" "+ diserang.getHp();
                    }
                }
            }
        }
        return result;
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName){
        System.out.println("==============================================================");
        String temp = "";
        for(Player pesulap:player){
            if(pesulap.getName().equalsIgnoreCase(meName)){
                for(Player gosong:player){
                    if(gosong.getName().equals(enemyName)){
                        gosong.setHp(gosong.getHp()-10);
                        gosong.setBurned();
                        if(gosong.getHp()<=0){
                            gosong.setDead();
                        }
                        temp += "Nyawa "+ gosong.getName()+" "+gosong.getHp()+" dan matang";
                    }

                }
            }
        }
        return temp;
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName){
        System.out.println("==============================================================");
        String result = "";
        boolean kemakan = false;
        Player bakalKemakan = null;
        for(Player pemakan:player){
            if (pemakan.getName().equals(meName)){
                for(Player dimakan:player){
                    if(dimakan.getName().equals(enemyName)){
                        if (dimakan.getIsAlive()==true){
                            result+= meName + " tidak bisa memakan " + enemyName;
                        } else {
                            if (pemakan.canEat(dimakan)){
                                pemakan.generalEat(dimakan);
                                kemakan = true;
                                bakalKemakan = dimakan;

                                result += pemakan.getName()+ " memakan "+ dimakan.getName()+", Nyawa "+pemakan.getName()+" kini "+pemakan.getHp();
                            } else {
                                result+= meName + " tidak bisa memakan " + enemyName;
                            }
                        }
                    }
                }
            }
        }
        if (kemakan == true) {
            int indexToRemove = player.indexOf(bakalKemakan);
            player.remove(indexToRemove);
            if(bakalKemakan.getTipe().equalsIgnoreCase("monster")) monster.remove(monster.indexOf(bakalKemakan));
        }
        return result;
    }


     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName){
        System.out.println("==============================================================");
        boolean adaGa= false;
        String result = "";
        for(Player hmAdaGakYa:player){
            if(hmAdaGakYa.getName().equalsIgnoreCase(meName)){
                adaGa = true;
            }
        }
        if(adaGa){
            for(Player pengenTeriak:player){
                if(pengenTeriak.getName().equalsIgnoreCase(meName)){
                    if (pengenTeriak.getTipe().equalsIgnoreCase("monster")){
                        for(Monster goblok:monster){
                            if(goblok.getName().equalsIgnoreCase(meName)){
                                result += goblok.roaring();
                            }
                        }
                    } else {
                        result+= meName + " tidak bisa berteriak";
                    }
                }
            }
        } else result+= "Tidak ada " + meName;

            return result;
        }
    }
