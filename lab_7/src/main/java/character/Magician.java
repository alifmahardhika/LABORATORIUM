package character;

//  write Magician Class here
public class Magician extends Player{
    public Magician(String name, int hp){
        super(name, "Magician", hp);
    }
    //methods
    public void burning(Player burned){
        burned.setBurned();
        burned.setHp(burned.getHp()-10);
        if(burned.getTipe().equals("Magician")){
            burned.setHp(burned.getHp()-10);
        }
        if(burned.getHp()<=0) {
            burned.setDead();
            burned.setHp(0);
        }
        System.out.format("%s membakar %s\n", getName(), burned.getName());
    }
    public void eating(Player dimakan){
        if(canEat(dimakan) == true){
            if (isAlive){
                if (dimakan.getIsAlive() == true){
                    System.out.format("%s is not dead yet\n", dimakan.getName());
                } else {
                    generalEat(dimakan);
                    System.out.format("%s memakan %s\n",getName(), dimakan.getName());
                }
            } else {
                System.out.format("%s is already dead\n", getName());
            }
        } else System.out.println("GABISA MAKAN GINIAN");

    }
}
