package character;
import java.util.ArrayList;
//  write Player Class here
public class Player{
    protected String name;
    protected int hp;
    protected ArrayList<Player> diet = new ArrayList<Player>();
    protected String tipe;
    protected boolean isAlive = true;
    protected String dietAkhir = "memakan ";
    protected boolean burnStatus = false;
    protected ArrayList<String> typeList = new ArrayList<String>();



    public Player(String name, String tipe, int hp){
        this.name = name;
        this.hp = hp;
        this.tipe = tipe;
    }

    public String getName(){
        return name;
    }
    public int getHp(){
        return hp;
    }
    public void setHp(int hp){
        this.hp = hp;
    }
    public String getTipe(){
        return tipe;
    }
    public boolean getIsAlive(){
        return isAlive;
    }

    public void setDead(){
        this.isAlive = false;
        this.setHp(0);
    }
    public void generalAttack(Player attacked){
        if (attacked.getHp() <=10){
            attacked.setDead();
        } else {
            attacked.setHp(attacked.getHp()-10);
            if (attacked.getTipe().equals("Magician")){
                attacked.setHp(attacked.getHp()-10);
                if (attacked.getHp() <=0) attacked.setDead();
            }
        }
    }
    public boolean getBurnStatus(){
        return burnStatus;
    }
    public void setBurned(){
        this.burnStatus = true;
    }

    //methods
    public String generalEat(Player eaten){
        this.hp += 15;
        this.diet.add(eaten);
        this.typeList.add(eaten.getTipe());
        return getName() + " memakan "+ eaten.getName();

    }
    public String diet(){
        String result = "";
        for(Player siapa:diet){
            if(diet.indexOf(siapa)+2<diet.size()){
                result+= siapa.getTipe()+" "+siapa.getName()+"";
            } else {
                result+= siapa.getTipe()+" "+siapa.getName();
            }

        }
        return result;
    }
    public String status(){
        String status= "";
        String strDiet = "";
        if (diet.size() > 0) strDiet +=  "Memakan " + diet();
        else strDiet += "Belum memakan siapa siapa";
        if (isAlive)status += "Masih Hidup";
        else status += "Sudah meninggal dengan damai";
        return getTipe() +" "+ getName() +"\nHP: "+ getHp() + "\n" + status +"\n"+ strDiet;
        //System.out.format("%s %s\nHP: %d\n%s\n%s\n\n", getTipe(), getName(), getHp(), status, strDiet);
    }
    public boolean canEat(Player food){
        if (food.isAlive==false){
            if(getTipe().equalsIgnoreCase("Monster")){
                return true;
            } else {
                if(food.getTipe().equalsIgnoreCase("Monster")){
                    if(food.getBurnStatus()==true) return true;
                    else return false;
                }
                else return false;
            }
        }
        else return false;
    }
}
