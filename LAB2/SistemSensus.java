import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author Alif Mahardhika, NPM 1706021934, Kelas E, GitLab Account: alifmahardhika
 */

public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner scanner = new Scanner(System.in);

		while (true){
		System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
				"--------------------\n" +
				"Nama Kepala Keluarga   : ");
		String nama = scanner.nextLine();

		System.out.print("Alamat Rumah           : ");
		String alamat = scanner.nextLine();

		System.out.print("Panjang Tubuh (cm)     : ");
		int panjang = (int) scanner.nextInt();
		double panjangMeter = (double) panjang/100;
		if (!(panjang>0 && panjang<=250)){
			System.out.println("WARNING: Keluarga tidak perlu direlokasi!");
			break;
		}

		System.out.print("Lebar Tubuh (cm)       : ");
		int lebar = (int) scanner.nextInt();
		double lebarMeter = (double) lebar/100;
		if (!(lebar>0 && lebar<=250)){
			System.out.println("WARNING: Keluarga tidak perlu direlokasi!");
			break;
		}

		System.out.print("Tinggi Tubuh (cm)      : ");
		int tinggi = (int) scanner.nextInt();
		double tinggiMeter = (double) tinggi/100;
		if (!(tinggi>0 && tinggi<=250)){
			System.out.println("WARNING: Keluarga tidak perlu direlokasi!");
			break;
		}

		System.out.print("Berat Tubuh (kg)       : ");
		double berat = (double) scanner.nextInt();
		if (!(berat>0.0 && berat<=150.0)){
			System.out.println("WARNING: Keluarga tidak perlu direlokasi!");
			break;
		}

		System.out.print("Jumlah Anggota Keluarga: ");
		int anggota = scanner.nextInt();
		if (!(anggota>0 && anggota<=20)){
			System.out.println("WARNING: Keluarga tidak perlu direlokasi!");
			break;
		}

		System.out.print("Tanggal Lahir          : ");
		String tanggalLahir = scanner.next();
		String temp = scanner.nextLine();

		System.out.print("Catatan Tambahan       : ");
		String catatan = scanner.nextLine();

		System.out.print("Jumlah Cetakan Data    : ");
		int jumlahCetakan = scanner.nextInt();

		//RASIO
		double volume = (double) (tinggiMeter*lebarMeter*panjangMeter);
		double rasio = (berat/volume);
		int rasioBulat = (int)rasio;

			// PERIKSA CATATAN
			if (catatan.length() > 0){ catatan = catatan;
			}else{
				catatan = "Tidak ada catatan" ;
			}

			//HASILNYA YAYY
			for (int n = 0; n < jumlahCetakan; n++) {
				System.out.print("\nPencetakan " + (n+1) + " dari " + jumlahCetakan + " untuk: ");
				String penerima = scanner.next(); // Lakukan baca input lalu langsung jadikan uppercase
				System.out.print("DATA SIAP DICETAK UNTUK ");
				System.out.print(penerima.toUpperCase()+"\n"+"--------------------"+"\n");
				System.out.println(nama + " - " + alamat);
				System.out.println("Lahir pada tanggal " + tanggalLahir);
				System.out.println("Rasio berat per volume = " + rasioBulat + " kg/m^3");
				if (catatan.isEmpty()){
					System.out.println("Tidak ada catatan tambahan.");
				} else {
					System.out.println(catatan);
				}
			}


//APARTEMEN
		//NOMOR KELUARGA
		char huruf = nama.charAt(0);																														//AMBIL KARAKTER PERTAMA STRING
		int totalNama = 0;
		for (int n = 0; n < (nama.length()); n++){
			char nilai= nama.charAt(n);
			int ascii = (int)nilai;
      totalNama += ascii;
		}int totalNilai = (int)((panjang*tinggi*lebar)+totalNama)%10000;												//RUMUS NOMOR KELUARGA
		String result = String.format("%d", totalNilai);
		String nomorKeluarga = (huruf + result);																								//NOMOR KELUARGA

		// ANGGARAN MAKANAN
		int anggaran = (50000*365*anggota);

		//UMUR
		int tahun = Integer.parseInt(tanggalLahir.substring(6, 10));
		int umur = 2018 - tahun;

		//REKOMENDASI APARTEMEN
		System.out.println("\n\nREKOMENDASI APARTEMEN\n--------------------");
		System.out.format("MENGETAHUI: Identitas Keluarga: %s - %s\n", nama, nomorKeluarga);
		System.out.format("MENIMBANG: Anggaran makanan tahunan: Rp. %d\n",anggaran);
		System.out.format("Umur kepala keluarga: %d tahun\n", umur);
		System.out.format("Memutuskan: Keluarga %s akan ditempatkan di:\n",nama);
		if (umur>0 && umur<=18){
			System.out.println("PPMT, kabupaten Rotunda");
		} else{
				if (anggaran>0 && anggaran<=100000000){
					System.out.println("Teksas, kabupaten Sastra");
				} else{
					System.out.println("Mares, kabupatern Margonda");
				}
		  }
		System.out.println("");
		break;
	}
		scanner.close();
	 }
 }
