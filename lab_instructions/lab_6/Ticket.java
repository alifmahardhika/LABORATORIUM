public class Ticket {
    //ATTRIBUT & CONSTRUCTORS
    private String judul;
    private String hari;
    private boolean tipe;
    private int hargaTiket;
    private static final int tiketNormal = 60000;

    public Ticket(String judul, String hari, boolean tipe){
        this.judul = judul;
        this.hari = hari;
        this.tipe = tipe;
    }
    public String getTipe(){
        if (this.tipe == true){
            return "3D";
        }else{
            return "Biasa";
        }
    }
    public void setHarga(String judul, String hari, boolean tipe){
        this.judul = judul;
        this.hari = hari;
        this.tipe = tipe;
    }
    public int getHarga(){
        this.hargaTiket += tiketNormal;
        if (this.hari == "Sabtu"|| hari == "Minggu"){
            hargaTiket += 40000;
        }
        if (this.tipe == true){
            Double biaya3D = (0.2*hargaTiket);
            hargaTiket += biaya3D;
        }
        return hargaTiket;
    }
    //METHODS
    public void printTiket(){
        System.out.format("----------------------------------------------\nFilm" +
        "\t\t: %s\nJadwal Tayang\t: %s\nJenis\t\t: %s\n-----------------------------------------------\n", judul, hari, getTipe());
    }
}
