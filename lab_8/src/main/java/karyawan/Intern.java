package karyawan;

public class Intern extends Karyawan {

	public Intern(String nama, int gaji) {
		super("Intern", nama, gaji);
	}
	
	public String rekrutBawahan(Karyawan bawahan) {
		return "Anda tidak layak memiliki bawahan";
	}
}