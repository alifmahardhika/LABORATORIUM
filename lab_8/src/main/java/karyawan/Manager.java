package karyawan;
import java.util.ArrayList;

public class Manager extends Karyawan {

	private ArrayList<Karyawan> listBawahan = new ArrayList<Karyawan>();	

	public Manager(String nama, int gaji) {
		super("Manager", nama, gaji);
	}

	public String rekrutBawahan(Karyawan bawahan) {
		String hasil = "";
		if (!bawahan.getTipe().equalsIgnoreCase("Manager")) {
			if (listBawahan.size() <= 10) {
				if (listBawahan.size() == 0) {
					listBawahan.add(bawahan);
					hasil += "Karyawan " + bawahan.getNama() + " berhasil ditambahkan menjadi bawahan " + this.getNama();
				} else {
					for (Karyawan cek : listBawahan) {
						if (cek.getNama().equalsIgnoreCase(bawahan.getNama())) {
							hasil += "Karyawan " + bawahan.getNama() + " telah menjadi bawahan " + this.getNama();
						} else {
							hasil += "Karyawan " + bawahan.getNama() + " berhasil ditambahkan menjadi bawahan " + this.getNama();
						}
					}
				}
			} else {
				hasil += this.getNama() + " sudah memiliki 10 bawahan";
			}
		} else {
			hasil += "Anda tidak layak memiliki bawahan";
		}
		return hasil;
	}
}