package karyawan;

public abstract class Karyawan {

	protected String nama, tipe;
	protected int gaji, gajianCount;

	public Karyawan(String tipe, String nama, int gaji) {
		this.tipe = tipe;
		this.nama = nama;
		this.gaji = gaji;
	}

	public String getNama() {
		return nama;
	}

	public String getTipe() {
		return tipe;
	}

	public int getGaji() {
		return gaji;
	}

	public int getGajianCount() {
		return gajianCount;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public void setGaji(int gajiBaru) {
		this.gaji = gajiBaru;
	}

	public void addGajianCount(){
        this.gajianCount += 1;
    }

	public String gajianBonus() {
        String hasil = "";
        int gajiSebelum = gaji;
        int gajiSesudah = gaji + (gaji * 10 / 100);
        setGaji(gajiSesudah);
        hasil += "\n" + this.getNama() + " mengalami kenaikan gaji sebesar 10% dari " + gajiSebelum + " menjadi " + gajiSesudah;
        return hasil;
	}

	public abstract String rekrutBawahan(Karyawan bawahan);
}