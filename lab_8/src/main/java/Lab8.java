import java.util.Scanner;

class Lab8 {
    public static void main(String[] args) {
    	Scanner scan = new Scanner(System.in);
        Perusahaan tampan = new Perusahaan();
        while (true) {
            if (scan.hasNextInt()) {
                tampan.batasPromosi(scan.nextLine());
            } else {
                String[] input = scan.nextLine().split(" ");
                if (input[0].equalsIgnoreCase("tambah_karyawan")) {
                    System.out.println(tampan.tambahKaryawan(input[1], input[2], input[3]));
                } else if (input[0].equalsIgnoreCase("status")) {
                    System.out.println(tampan.status(input[1]));
                } else if (input[0].equalsIgnoreCase("tambah_bawahan")) {
                    System.out.println(tampan.tambahBawahan(input[1], input[2]));
                } else if (input[0].equalsIgnoreCase("gajian")) {
                    System.out.println(tampan.gajian());
                } else if (input[0].equalsIgnoreCase("exit")) {
                    break;
                } else {
                    System.out.println("Perintah salah");
                }
            }
        }
    }
}
