import java.util.ArrayList;
import karyawan.Intern;
import karyawan.Karyawan;
import karyawan.Manager;
import karyawan.Staff;



public class Perusahaan {
	private static ArrayList<Karyawan> karyawan = new ArrayList<Karyawan>();
	private static int batasPromosi;

	public void batasPromosi(String batasBaru) {
        batasPromosi = Integer.parseInt(batasBaru);
    }

	public Karyawan terdaftar(String nama) {
        for (Karyawan cari : karyawan) {
            if (cari.getNama().equalsIgnoreCase(nama)) {
                return cari;
            }
        }
        return null;
    }

	public String tambahKaryawan(String nama, String tipe, String gaji) {
        String hasil = "";
        if (karyawan.size() == 10000){
            hasil += "Jumlah pegawai sudah melebihi kapasitas 10000 karyawan";
        } else {
            if (terdaftar(nama) != null) {
                return "Karyawan dengan nama " + nama + " telah terdaftar";
            } else {
                if (tipe.equalsIgnoreCase("intern")) {
                    Intern intern = new Intern(nama, Integer.parseInt(gaji));
                    karyawan.add(intern);
                    hasil += intern.getNama() + " mulai bekerja sebagai " + intern.getTipe() + " di PT. TAMPAN";
                }
                else if (tipe.equalsIgnoreCase("Staff")){
                    Staff staff = new Staff(nama, Integer.parseInt(gaji));
                    karyawan.add(staff);
                    hasil += staff.getNama() + " mulai bekerja sebagai " + staff.getTipe() + " di PT. TAMPAN";
                }
                else if (tipe.equalsIgnoreCase("Manager")){
                    Manager manager = new Manager(nama, Integer.parseInt(gaji));
                    karyawan.add(manager);
                    hasil += manager.getNama() + " mulai bekerja sebagai " + manager.getTipe() + " di PT. TAMPAN";
                }
            }
        }
        return hasil;
    }

    public String status(String nama) {
        if (terdaftar(nama) != null) {
            Karyawan pekerja = terdaftar(nama);
            return pekerja.getNama() + " " + pekerja.getGaji();
        } else {
        	return "Karyawan tidak ditemukan";
        }
    }

    public String tambahBawahan(String merekrut, String direkrut){
        if (terdaftar(merekrut) != null) {
            Karyawan atasan = terdaftar(merekrut);
            if (terdaftar(direkrut) != null) {
                    Karyawan bawahan = terdaftar(direkrut);
                    return atasan.rekrutBawahan(bawahan);
            } else {
                return "Nama tidak berhasil ditemukan";
            }
        } else {
        	return "Nama tidak berhasil ditemukan";
        }
    }

    public String gajian() {
        String hasil = "";
        if (karyawan.size() > 0) {
        	hasil += "Semua karyawan telah diberi gaji";
            for (Karyawan semua : karyawan) {
            	semua.addGajianCount();
                if (semua.getGajianCount() % 6 == 0) {
                    hasil += semua.gajianBonus();
                    if (semua.getTipe().equalsIgnoreCase("Staff") && semua.getGaji() >= batasPromosi) {
                        karyawan.set(karyawan.indexOf(semua), new Manager(semua.getNama(), semua.getGaji()));
                        hasil += "\nSelamat, "+ semua.getNama() + " telah dipromosikan menjadi MANAGER";
                    }
                }
            }
        } else {
        	hasil += "Tidak ada karyawan";
        }
        return hasil;
    }
}
