/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Anda diwajibkan untuk mengimplementasikan method yang masih kosong
 * Anda diperbolehkan untuk menambahkan method jika dibutuhkan
 * HINT : Bagaimana caranya cek apakah sudah menang atau tidak? Mungkin dibutuhkan method yang bisa membantu? Hmmmm.
 * Semangat ya :]
 */
 //Nama: Alif Mahardhika; NPM: 1706021934; MASIH NGETEST

public class BingoCard {

	private Number[][] numbers;
	private Number[] numberStates;
	private boolean isBingo;

	public BingoCard(Number[][] numbers, Number[] numberStates) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
	}

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates() {
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}

	public boolean isBingo() { return isBingo; }

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

	public String markNum(int num){
		if(this.numberStates[num] == null){
			return "Kartu tidak memiliki angka " + num;
		}
		if(this.numberStates[num].isChecked()){
			return num + " sebelumnya sudah tersilang";
		}
		if(!(this.numberStates[num].isChecked())){
			int x = this.numberStates[num].getX();
			int y = this.numberStates[num].getY();
			this.numbers[x][y].setChecked(true);
			this.numberStates[num].setChecked(true);
			this.checkBingo();
			return num + " tersilang";
		}
		else{
			this.checkBingo();
			return "terjadi kesalahan";
		}
	}

	public String info(){
		String hasil = "";
		for(int x = 0; x < 5; x++){
			hasil +=("|");
			for(int y = 0; y < 5; y++){
				if(this.numbers[x][y].isChecked()){
					hasil += (" X  |");
				}
				else{
					if(this.numbers[x][y].getValue() >= 10){
						hasil += (" " +this.numbers[x][y].getValue() + " |");
					}
					if(this.numbers[x][y].getValue() < 10){
						hasil += (" 0" + this.numbers[x][y].getValue() + " |");
					}
				}
			}
			if(x != 4){hasil += "\n";}
		}
		return hasil;
	}

	public void restart(){
		for(int x = 0; x < 5; x++){
			for(int y = 0; y < 5; y++){
				if(this.numbers[x][y].isChecked()){
					this.numbers[x][y].setChecked(false);
					this.numberStates[this.numbers[x][y].getValue()].setChecked(false);
				}
			}
		}
		System.out.println("Mulligan!");
	}

	public void checkBingo(){
		//VERTIKAL
		for(int x = 0; x < 5; x++){
			if(this.numbers[x][0].isChecked() && this.numbers[x][1].isChecked() && this.numbers[x][2].isChecked() && this.numbers[x][3].isChecked() && this.numbers[x][4].isChecked()){
			    setBingo(true);
			}
			if(this.numbers[0][x].isChecked() && this.numbers[1][x].isChecked() && this.numbers[2][x].isChecked() && this.numbers[3][x].isChecked() && this.numbers[4][x].isChecked()){
			    setBingo(true);
			}
		}
		//DIAGONAL
		if (this.numbers[0][0].isChecked() && this.numbers[1][1].isChecked() && this.numbers[2][2].isChecked() && this.numbers[3][3].isChecked() && this.numbers[4][4].isChecked()) {
		    setBingo(true);
		}
		//DIAGONAL
		if (this.numbers[0][4].isChecked() && this.numbers[1][3].isChecked() && this.numbers[2][2].isChecked() && this.numbers[3][1].isChecked() && this.numbers[4][0].isChecked()){
		    setBingo(true);
		}
	}


}
