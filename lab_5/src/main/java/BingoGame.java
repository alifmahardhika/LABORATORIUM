import java.util.Scanner;

public class BingoGame {
    public static void main(String[] args) {
        boolean game = true;
        System.out.println("AYO MAIN BINGO!\nBuat kartu Bingo dengan memasukkan lima angka (0-99) perbaris!\nTekan Enter untuk baris berikutnya!");
        Scanner input = new Scanner(System.in);
        String Baris1 = input.nextLine();
        String Baris2 = input.nextLine();
        String Baris3 = input.nextLine();
        String Baris4 = input.nextLine();
        String Baris5 = input.nextLine();
        String[] splitBaris1 = Baris1.split(" ");
        String[] splitBaris2 = Baris2.split(" ");
        String[] splitBaris3 = Baris3.split(" ");
        String[] splitBaris4 = Baris4.split(" ");
        String[] splitBaris5 = Baris5.split(" ");
        Number[][] bingo = new Number[5][5];
        for (int x = 0; x < 5; x++) {
            int a = Integer.parseInt(splitBaris1[x]);
            bingo[0][x] = new Number(a, 0, x);
        }
        for (int x = 0; x < 5; x++) {
            int a = Integer.parseInt(splitBaris2[x]);
            bingo[1][x] = new Number(a, 1, x);
        }
        for (int x = 0; x < 5; x++) {
            int a = Integer.parseInt(splitBaris3[x]);
            bingo[2][x] = new Number(a, 2, x);
        }
        for (int x = 0; x < 5; x++) {
            int a = Integer.parseInt(splitBaris4[x]);
            bingo[3][x] = new Number(a, 3, x);
        }
        for (int x = 0; x < 5; x++) {
            int a = Integer.parseInt(splitBaris5[x]);
            bingo[4][x] = new Number(a, 4, x);
        }
        Number[] states = new Number[100];
        for(int i=0; i<5; i++){
            for(int j=0; j<5; j++){
                states[bingo[i][j].getValue()] = bingo[i][j];
            }
        }
        BingoCard card = new BingoCard(bingo, states);
        System.out.println("Permainan dimulai!");
        while(game){
            System.out.println("Masukkan Perintah Berikutnya");
            String command = input.next();
            if(command.equals("MARK")){
                int num = input.nextInt();
                if (num>=100){
                  System.out.println("Angka tidak boleh lebih dari 99");
                }
                System.out.println(card.markNum(num));
            }
            else if(command.equals("INFO")){
                System.out.println(card.info());
            }
            else if(command.equals("EXIT")){
                break;
            }
            else if(command.equals("RESTART")){
                card.restart();
            }
            if(card.isBingo()){
                System.out.println("BINGO");
                System.out.println(card.info());
                break;
            }
        }
    }
}
