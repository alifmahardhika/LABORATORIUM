//Alif Mahardhika; 1706021934
package lab9.event;

import java.math.BigInteger;
import java.util.Calendar;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
* A class representing an event and its properties
*/
public class Event implements Comparable<Event>{
    /** creating String type variables that will be needed */
    protected  String name, startEvent, endEvent, startDay, endDay, startTime,
    endTime, startResult, endResult;
    /** Event cost */
    protected  BigInteger cost;
    /** Calendar */
    protected Calendar start = Calendar.getInstance();
    protected Calendar end = Calendar.getInstance();
    protected  int year, month, day;
    /** Constructor */
    public Event(String name, String startEvent, String endEvent, String strCost) {
        this.name = name;
        this.startEvent = startEvent;
        this.endEvent = endEvent;
        String[] startSplit = startEvent.split("_");
        this.startDay = startSplit[0];
        this.startTime = startSplit[1];
        String[] endSplit = endEvent.split("_");
        this.endDay = endSplit[0];
        this.endTime = endSplit[1];
        this.cost = new BigInteger(strCost);
        this.setCalendar(startDay, startTime, "start");
        this.setCalendar(endDay, endTime, "end");

    }
    /**
    * @return BigInteger type cost
    */
    public BigInteger getCost() {
        return cost;
    }

    /**
    * setting the calendatr
    * @param String type object of day, time, adn type (start or end)
    */
    public void setCalendar(String day, String time, String type) {
        String[] daySplit = day.split("-");
        String[] timeSplit = time.split(":");
        if (type.equals("start")) {
            this.startResult = daySplit[2] + "-" + daySplit[1] + "-" + daySplit[0] + ", " +
            timeSplit[0] + ":" + timeSplit[1] + ":" + timeSplit[2];
            start.set(Integer.parseInt(daySplit[0]), Integer.parseInt(daySplit[1]),
                Integer.parseInt(daySplit[2]), Integer.parseInt(timeSplit[0]),
                Integer.parseInt(timeSplit[1]), Integer.parseInt(timeSplit[2]));
        } else {
            this.endResult = daySplit[2] + "-" + daySplit[1] + "-" + daySplit[0] +
            ", " + timeSplit[0] + ":" + timeSplit[1] + ":" + timeSplit[2];
            end.set(Integer.parseInt(daySplit[0]), Integer.parseInt(daySplit[1]),
            Integer.parseInt(daySplit[2]), Integer.parseInt(timeSplit[0]),
            Integer.parseInt(timeSplit[1]), Integer.parseInt(timeSplit[2]));
        }
    }

    /**
    * @return start time of event in string type
    */
    public String getStart() {
        DateFormat formatted = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        return formatted.format(start.getTime());
    }

    /**
    * @return end time of event in string type
    */
    public String getEnd() {
        DateFormat formatted = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        return formatted.format(end.getTime());
    }

    /**
    * @return end time of event in Calendar type object
    */
    public Calendar getEndCal() {
        return end;
    }

    /**
    * @return start time of event in Calendar type object
    */
    public Calendar getStartCal() {
        return start;
    }

    /**
    * Accessor for name field.
    * @return name of this event instance
    */
    public String getName() {
        return this.name;
    }

    /**
    * method to print Event type object
    * @return formatted information of Event
    */
    public String toString() {
        return this.name +
               "\nWaktu mulai: " + startResult +
               "\nWaktu selesai: " + endResult +
               "\nBiaya kehadiran: " + this.cost;
    }

    /**
    * accessor for start startDay
    * @return startDay in string type
    */
    public String getStartDay() {
        return this.startDay;
    }

    /**
    * accessor for start startTime
    * @return startTime in string type
    */
    public String getStartTime() {
        return this.startTime;
    }

    /**
    * accessor for start endDay
    * @return endDay in string type
    */
    public String getEndDay() {
        return this.endDay;
    }

    /**
    * accessor for start endTime
    * @return endTime in string type
    */
    public String getEndTime() {
        return this.endTime;
    }

    @Override
    public int compareTo(Event other) {
        return this.getStartCal().compareTo(other.getStartCal());
    }
}
