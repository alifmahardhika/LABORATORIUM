//Alif Mahardhika; 1706021934
package lab9.user;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import lab9.event.Event;

/**
* Class representing a user, willing to attend event(s)
*/
public class User {
    /** Name of user */
    private String name;

    /** List of events this user plans to attend */
    private ArrayList<Event> events;

    /** total cost */
    private BigInteger totalCost = new BigInteger("0");

    /**
    * Constructor
    * Initializes a user object with given name and empty event list
    */
    public User(String name) {
        this.name = name;
        this.events = new ArrayList<Event>();
    }

    /**
    * Accessor for name field
    * @return name of this instance
    */
    public String getName() {
        return name;
    }
    /** set the value of cost */
    public void setCost(BigInteger cost) {
        this.totalCost = cost;
    }

    /**
    * Adds a new event to this user's planned events, if not overlapping
    * with currently planned events.
    *
    * @return true if the event if successfully added, false otherwise
    */
    public boolean addEvent(Event newEvent) {
        boolean booked = false;
        boolean result = false;
        if (events.size() == 0) {
            result = true;
            events.add(newEvent);
        } else {
            for (Event check : events) {
                if (isBooked(newEvent, check)){
                    booked = true;
                }
            }
            if (booked == true) {
                result = false;
            } else {
                events.add(newEvent);
                result = true;
            }
        }
        return result;
    }
    /**
    *checking for overlapping event schedules
    @return true if schedule for given time is booked
    */
    public boolean isBooked(Event first, Event latter) {
        if (first.getStartCal().after(latter.getEndCal()) || first.getEndCal().before(latter.getStartCal())) {
            return false;
        } else {
            return true;
        }
    }

    /**
    * Returns the list of events this user plans to attend,
    * Sorted by their starting time.
    * Note: The list returned from this method is a copy of the actual
    *       events field, to avoid mutation from external sources
    *
    * @return list of events this user plans to attend
    */
    public ArrayList<Event> getEvents() {
        ArrayList<Event> copyArray = new ArrayList<Event>();
        for (Event toCopy : this.events) {
            copyArray.add(toCopy);
        }
        Collections.sort(copyArray, (start1, start2) -> start1.getStartCal().compareTo(start2.getStartCal()));
        // WARNING: The list returned needs to be a copy of the actual events list.
        //          You don't want people to change your plans (e.g. clearing the
        //          list) without your consent, right?
        // HINT: see Java API Documentation on ArrayList (java.util.ArrayList)                                                                        (or... Google. Yeah that works too. OK.)
        return copyArray;
    }

    /**
    *summing total cost for every assigned events
    */
    public BigInteger getTotalCost() {
        for (Event booked : events) {
            setCost(this.totalCost.add(booked.getCost()));
        }
        return this.totalCost;
    }
}
