//Alif Mahardhika; 1706021934
package lab9;

import java.util.ArrayList;
import lab9.event.Event;
import lab9.user.User;

/**
* Class representing event managing system
*/
public class EventSystem {
    /**
    * List of events
    */
    private ArrayList<Event> events;

    /**
    * List of users
    */
    private ArrayList<User> users;

    /**
    * Constructor. Initializes events and users with empty lists.
    */
    public EventSystem() {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    /**
    * Adding an event to the System
    *@return a string result to tell if the addition is successfull
    */
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr) {
        boolean success = true;
        String hasil = "";
        if (events.size() == 0) {
            success = true;
        } else {
            for (Event check : events) {
                if (check.getName().equalsIgnoreCase(name)) {
                    hasil += "Event " + name + " sudah ada!";
                    success = false;
                }
            }
        }

        if (success == true) {
            Event event = new Event(name, startTimeStr, endTimeStr, costPerHourStr);
            if (event.getStartCal().after(event.getEndCal()) || startTimeStr.equalsIgnoreCase(endTimeStr)) {
                hasil += "Waktu yang diinputkan tidak valid!";
            } else {
                hasil += "Event " + name + " berhasil ditambahkan!";
                events.add(event);
            }

        }
        return hasil;
    }

    /**
    * Adding a user to the System
    *@return a string result to tell if the addition is successfull
    */
    public String addUser(String name) {
        String hasil = "";
        User toBeAdded = getUser(name);
        if (toBeAdded == null) {
            User added = new User(name);
            this.users.add(added);
            hasil += "User " + name + " berhasil ditambahkan!";
        } else {
            hasil += "User " + name + " sudah ada!";
        }
        return hasil;
    }

    /**
    * registering a user to attend an event
    *@return a string result to tell if the registration is successfull
    */
    public String registerToEvent(String userName, String eventName) {
        String hasil = "";
        User user = this.getUser(userName);
        Event event = this.getEvent(eventName);
        if (user == null && event == null) {
            hasil += "Tidak ada pengguna dengan nama " + userName + " dan acara dengan nama " + eventName + "!";
        } else if (user == null) {
            hasil += "Tidak ada pengguna dengan nama " + userName + "!";
        } else if (event == null) {
            hasil += "Tidak ada acara dengan nama " + eventName + "!";
        } else {
            if (user.addEvent(event)) {
                hasil += userName +" berencana menghadiri " + eventName + "!";
            } else {
                hasil += userName + " sibuk sehingga tidak dapat menghadiri " + eventName + "!";
            }
        }
        return hasil;
    }

    /**
    * method to get user with such name
    * @param String of requested userName
    * @return a User type object if there is a user with such name, null if there is none
    */
    public User getUser(String userName) {
        User hasil = null;
        for (User user : users) {
            if (user.getName().equalsIgnoreCase(userName)) {
                hasil = user;
            }
        }
        return hasil;
    }

    /**
    * method to get event with such name
    * @param String  of requested eventName
    * @return an Event type object if there is a event with such name, null if there is none
    */
    public Event getEvent(String eventName) {
        Event hasil = null;
        for (Event event : events) {
            if (event.getName().equalsIgnoreCase(eventName)) {
                hasil = event;
            }
        }
        return hasil;
    }
}
