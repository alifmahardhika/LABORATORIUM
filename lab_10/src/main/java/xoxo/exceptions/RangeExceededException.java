package xoxo.exceptions;
import javax.swing.*;

/**
 * An exception that is thrown if the range of seed 
 * is less than 0 or more than 36
 */
public class RangeExceededException extends RuntimeException{

    /**
     * Class constructor.
     */

    public RangeExceededException(String message) {
    	super(message);
    	JOptionPane.showMessageDialog(null, message);
    }


}