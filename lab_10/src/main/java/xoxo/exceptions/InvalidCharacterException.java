package xoxo.exceptions;
import javax.swing.*;

/**
 * The class handle the exception if the character doesn't match with the rule
 */
public class InvalidCharacterException extends RuntimeException{

	/**
     * Class constructor.
     */

    public InvalidCharacterException(String message) {
    	super(message);
    	JOptionPane.showMessageDialog(null, message);
    }

}