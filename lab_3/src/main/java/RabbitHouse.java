import java.util.Scanner;

public class RabbitHouse{

  public static int rekursi(int NameLength){                                    //HITUNG JUMLAH NORMAL
      int jumlah = 1;
      if (NameLength <=1){
        return 1;
      } else{
        return jumlah += NameLength*(rekursi(NameLength-1));
      }
  }

  public static boolean palindrome(String nama){                                //CEK APAKAH PALINDROME
      if (nama.length() == 1 || nama.length() == 0){
        return true;
      } else if (nama.charAt(0) == nama.charAt(nama.length()-1)){
          return palindrome(nama.substring(1, (nama.length()-1)));
      } else{
          return false;
      }
  }

  public static int anakgapalindrom(String nama){                               //HITUNG JUMLAH PALINDROM
      int jumlah = 0;
      if(!(palindrome(nama))){
        jumlah+=1;
        for(int n = 0; n<nama.length(); n++ ){
          jumlah += anakgapalindrom(nama.substring(0, n) + nama.substring(n+1, nama.length()));       //POTONG NAMA
        }
      }
      return jumlah;
  }

  public static void main(String[] args){                                       //MAIN FUNCTION
      Scanner input = new Scanner(System.in);
      System.out.print("TIPE DAN NAMA: ");                                      //MINTA INPUT
      String tipe = input.next();
      String nama = input.next();
      int InputLength = nama.length();
      String lower = tipe.toLowerCase();
      if (!(InputLength>10)) {
        if (lower.equals("normal")){                                              //JIKA TIPE NORMAL
          System.out.println(rekursi(InputLength));
          input.close();
        }else if(lower.equals("palindrom")){                                      //JIKA TIPE PALINDROM
          System.out.println(anakgapalindrom(nama));
        }else {
          System.out.println("Tipe tidak sesuai");                                //INPUT SELAIN NORMAL ATAU PALINDROM
        }
    }else{
      System.out.println("Nama tidak boleh lebih dari 10 karakter");
    }

  }
}
